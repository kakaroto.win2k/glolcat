# glolcat

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/kakaroto.win2k/glolcat.svg)](https://pkg.go.dev/gitlab.com/kakaroto.win2k/glolcat)

***GLolcat*** is an utility which concatenates like similar to cat command and adds rainbow coloring to it. 

## Description
GLolcat is primarily used for rainbow coloring of text in Terminal.

## Installation
***go install -v gitlab.com/kakaroto.win2k/glolcat@latest***

