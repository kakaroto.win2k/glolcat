module gitlab.com/kakaroto.win2k/glolcat

go 1.19

require github.com/mattn/go-colorable v0.1.13

require (
	github.com/mattn/go-isatty v0.0.16 // indirect
	golang.org/x/sys v0.1.0 // indirect
)
