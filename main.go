package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"

	"github.com/mattn/go-colorable"
	"gitlab.com/kakaroto.win2k/glolcat/glolcat"
)

func main() {

	version := flag.Bool("v", false, "Info version")
	//infoBuild, _ := debug.ReadBuildInfo()

	flag.Parse()

	if *version {
		printVersion(os.Stdout)
		os.Exit(0)

	}

	// Windows support
	defer colorable.EnableColorsStdout(nil)()
	info, _ := os.Stdin.Stat()
	var output []rune

	if info.Mode()&os.ModeCharDevice != 0 {
		fmt.Println("This command is intended to work with pipes.")
		fmt.Println("Usage: echo 'Hi! everyone say hi . ' | glolcat")
		os.Exit(0)
	}

	reader := bufio.NewReader(os.Stdin)
	for {
		input, _, err := reader.ReadRune()
		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)
	}

	glolcat.Print(output)
}
